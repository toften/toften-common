package net.toften.common.utils;

public abstract class EnumUtils {
	public static <E extends Enum<?>> E findValue(String valueName, E[] e) {
		E returnValue = null;
		
		for (E status : e) {
			if (status.name().equals(valueName)) {
				returnValue = status;
				break;
			}
		}
		
		return returnValue;
	}

}
