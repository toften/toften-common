package net.toften.common.utils;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

public abstract class CollectionsUtils {
	public static String toString(Collection<?> col, String del) {
		StringBuffer returnValue = new StringBuffer();
		
		for (Iterator<?> iterator = col.iterator(); iterator.hasNext();) {
			returnValue.append(iterator.next().toString());
			if (!iterator.hasNext())
				returnValue.append(del);
		}
		
		return returnValue.toString();
	}
	
	public static List<String> toStringCollection(Collection<?> col) {
		List<String> returnValue = new Vector<String>();
		
		for (Iterator<?> iterator = col.iterator(); iterator.hasNext();) {
			returnValue.add(iterator.next().toString());
		}
		
		return returnValue;
	}
	
	public static List<String> toStringCollection(String brokers, String del) {
		List<String> returnValue = new Vector<String>();
		
		for (String broker : brokers.split(del)) {
			returnValue.add(broker);
		}
		
		return returnValue;
		
	}

	public static String[] asArray(Collection<?> col) {
		String[] returnValue = new String[col.size()];
		int index = 0;

		for (Iterator<?> iterator = col.iterator(); iterator.hasNext();) {
			returnValue[index++] = iterator.next().toString();
		}
		
		return returnValue;
	}

}
