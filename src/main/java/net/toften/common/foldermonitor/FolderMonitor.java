package net.toften.common.foldermonitor;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Vector;

/**
 * This class will monitor a folder for change, and notify
 * any {@link FolderMonitorListener listeners} of any additions or
 * removals
 * 
 * @author thomas
 *
 */
public class FolderMonitor
	implements
		Runnable {
	private int 							interval = 5000;
	private boolean 						enabled = false;
	private File 							folderPath;
	private boolean 						running = true;
	private Thread 							monitorThread;
	private List<FolderMonitorListener> 	listeners = new Vector<FolderMonitorListener>();
	private List<String> 					folderFileList = new Vector<String>();

	/**
	 * This will create a new FolderMonitor.
	 * 
	 * Note, that the monitor will initially be disables. To start
	 * scanning the folder, the {@link #enable()} method must
	 * be invoked.
	 * 
	 * @param path the path to the folder to monitor
	 * @param interval the interval (in ms) of each scan of the folder
	 * @throws FileNotFoundException if the folder at the path is not found
	 * @throws IllegalArgumentException if the path is not to a folder
	 */
	public FolderMonitor(String path, int interval) throws FileNotFoundException {
		this.interval = interval;
		
		folderPath = new File(path);
		if (!folderPath.exists()) 
			throw new FileNotFoundException("Path " + path + " not found.");
		
		if (!folderPath.isDirectory())
			throw new IllegalArgumentException("Path " + path + " not a directory");
		
		monitorThread = new Thread(this);
		monitorThread.setPriority(Thread.NORM_PRIORITY - 1);
		monitorThread.start();
	}

	public synchronized void run() {
		while (running) {
			try {
				if (enabled) {
					scanFolder();
	
					Thread.sleep(interval);
				} else {
					wait();
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		
	}
	
	public void setThreadPriority(int newPriority) {
		monitorThread.setPriority(newPriority);
	}
	
	public void setScanInterval(int newInterval) {
		this.interval = newInterval;
	}

	public void scanFolder() {
		List<String> tempList = new Vector<String>(folderFileList);
		
		String[] files = folderPath.list();
		for (int i = 0; i < files.length; i++) {
			if (!folderFileList.contains(files[i])) { // File is new
				notifyNewFile(files[i]);
				folderFileList.add(files[i]);
			} else {
				tempList.remove(files[i]);
			}
		}
		
		for (String file : tempList) {
			folderFileList.remove(file);
			notifyFileRemoved(file);
		}
		
	}

	private void notifyFileRemoved(String string) {
		for (FolderMonitorListener listener : listeners) {
			listener.fileRemoved(string);
		}
		
	}

	private void notifyNewFile(String string) {
		for (FolderMonitorListener listener : listeners) {
			listener.fileAdded(string);
		}
		
	}

	public void addListener(FolderMonitorListener listener) {
		listeners.add(listener);
		
	}

	public void removeListener(FolderMonitorListener listener) {
		listeners.remove(listener);
		
	}
	
	/**
	 * This will enable the monitor.
	 * 
	 * Note, that when thid method is invoked the folder will be
	 * {@link #scanFolder() scanned} once immediately.
	 * 
	 * @see #disable()
	 */
	public synchronized void enable() {
		enabled = true;
		notify();
		scanFolder();
	}
	
	/**
	 * This will disable the monitor.
	 * 
	 * @see #enable()
	 */
	public synchronized void disable() {
		enabled = false;
	}
	
}
