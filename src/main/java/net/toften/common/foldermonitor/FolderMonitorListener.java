package net.toften.common.foldermonitor;

public interface FolderMonitorListener {

	void fileAdded(String fileName);

	void fileRemoved(String fileName);

}
