package net.toften.common.models;

import java.util.List;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

public class MyListModelWrapper<K> 
	extends 
		AbstractListModel 
	implements 
		ComboBoxModel {
	private List<K> list;
	
	K selection = null;
	
	public MyListModelWrapper(List<K> brokers) {
		this.list = brokers;
	}

	public K getSelectedItem() {
		return selection;
	}

	@SuppressWarnings("unchecked")
	public void setSelectedItem(Object anItem) {
		selection = (K)anItem;	
	}

	public K getElementAt(int index) {
		return list.get(index);
	}

	public int getSize() {
		return list.size();
	}

}
